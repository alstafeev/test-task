import org.testng.TestNG;
import tests.factory.TestOneFactory;
import tests.factory.TestTwoFactory;

public class Main {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        testNG.setTestClasses(new Class[]{TestOneFactory.class, TestTwoFactory.class});
        testNG.run();
    }
}
