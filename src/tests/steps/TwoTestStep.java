package tests.steps;

import base.BaseStep;
import base.exception.ChildElementNotFoundException;
import base.exception.ElementNotClickException;
import base.exception.ElementVisibleException;
import com.google.common.collect.Ordering;
import helpers.BaseMethods;
import io.qameta.allure.Step;
import models.ElementsMap;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

public class TwoTestStep extends BaseStep {

    public TwoTestStep(BaseMethods baseMethods, ElementsMap elementsMap) {
        super(baseMethods, elementsMap);
    }

    @Step(value = "Sort smartphones page by price")
    public void sortSmartphones() {
        try {
            Assert.assertTrue(baseMethods.clickElement(elementsUuid.get("sortPrice").getElementByXpath()));
        } catch (ElementNotClickException err) {
            Assert.fail(err.getMessage());
        }
    }

    @Step(value = "Check smartphones sort")
    public void checkSort() {
        try {
            baseMethods.clickElement(elementsUuid.get("viewButton").getElementByXpath());
            baseMethods.isElementNotExist(By.xpath("//*[contains(@class, 'preloadable__paranja')]"));
            List<WebElement> elements = baseMethods.findChildElements(elementsUuid.get("products").getElementByXpath(), "//*[contains(@id, 'product-')]");
            ArrayList<Integer> priceList = new ArrayList<>();
            for (WebElement element : elements) {
                priceList.add(Integer.parseInt(element.findElement(By.xpath("//*[contains(@class, 'n-snippet-card2__main-price-wrapper')]")).getText().split("₽")[0].replaceAll("\\s+", "")));
            }
            Assert.assertTrue(Ordering.natural().isOrdered(priceList));
        } catch (ChildElementNotFoundException | ElementVisibleException | ElementNotClickException err) {
            Assert.fail(err.getMessage());
        }
    }
}
