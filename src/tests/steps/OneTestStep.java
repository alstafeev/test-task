package tests.steps;

import base.BaseStep;
import base.exception.ElementNotVisibleException;
import helpers.BaseMethods;
import io.qameta.allure.Step;
import models.ElementsMap;
import org.testng.Assert;

public class OneTestStep extends BaseStep {

    public OneTestStep(BaseMethods baseMethods, ElementsMap elementsMap) {
        super(baseMethods, elementsMap);
    }

    @Step(value = "Checking visibility filters page")
    public void checkFilters() {
        try {
            Assert.assertTrue(baseMethods.isElementExist(elementsUuid.get("apply").getElementByXpath()) &&
                    baseMethods.isElementExist(elementsUuid.get("inputPriceFrom").getElementByXpath()) &&
                    baseMethods.isElementExist(elementsUuid.get("producer").getElementByXpath()));
        } catch (ElementNotVisibleException err) {
            Assert.fail(err.getMessage());
        }
    }
}