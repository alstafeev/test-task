package tests.factory;

import org.testng.annotations.Factory;
import tests.TestTwo;

public class TestTwoFactory {

    @Factory
    public Object[] factoryMethod() throws Exception {
        return new Object[] { new TestTwo() };
    }

}
