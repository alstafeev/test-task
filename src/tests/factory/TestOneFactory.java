package tests.factory;

import helpers.ResourceReader;
import models.InitData;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import tests.TestOne;

public class TestOneFactory {

    @Factory(dataProvider = "dataProvider")
    public Object[] factoryMethod(InitData data) throws Exception {
        return new Object[] { new TestOne(data) };
    }

    @DataProvider
    public Object[] dataProvider() throws Exception {
        return new ResourceReader().getInitData("initData.json");
    }

}
