package tests;

import base.BaseTest;
import io.qameta.allure.Description;
import org.testng.annotations.Test;
import tests.steps.TwoTestStep;

public class TestTwo extends BaseTest {

    private TwoTestStep testStep;

    public TestTwo() throws Exception {
        super();
        testStep = new TwoTestStep(baseMethods, resourceReader.getElementsByTest("two"));
    }

    @Test
    @Description(value = "Open yandex")
    public void openYandexPage() {
        testStep.stepOpenUrl("https://yandex.ru/");
        testStep.checkYandexPage();
    }

    @Test(dependsOnMethods = "openYandexPage")
    @Description(value = "Open yandex market")
    public void openYandexMarket() {
        testStep.stepOpenMarket("market");
        testStep.checkYandexMarket();
    }

    @Test(dependsOnMethods = "openYandexMarket")
    @Description(value = "Open electronics")
    public void openElectronics() {
        testStep.stepOpenPage("electronics");
        testStep.checkElectronics();
    }

    @Test(dependsOnMethods = "openElectronics")
    @Description(value = "Open smartphones")
    public void openSmartphones() {
        testStep.stepOpenPage("smartphones");
        testStep.checkSmartphones();
    }

    @Test(dependsOnMethods = "openSmartphones")
    @Description(value = "Sort product and check it")
    public void sortProduct() {
        testStep.sortSmartphones();
        testStep.checkSort();
    }
}
