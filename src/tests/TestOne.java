package tests;

import base.BaseTest;
import io.qameta.allure.*;
import models.InitData;
import org.testng.annotations.*;
import tests.steps.OneTestStep;

public class TestOne extends BaseTest {

    private OneTestStep testStep;

    private InitData initData;

    public TestOne(InitData initData) throws Exception {
        super();
        this.initData = initData;
        testStep = new OneTestStep(baseMethods, resourceReader.getElementsByTest("one"));
    }

    @Test
    @Description(value = "Open yandex")
    public void openYandexPage() {
        testStep.stepOpenUrl("https://yandex.ru/");
        testStep.checkYandexPage();
    }

    @Test(dependsOnMethods = "openYandexPage")
    @Description(value = "Open yandex market")
    public void openYandexMarket() {
        testStep.stepOpenMarket("market");
        testStep.checkYandexMarket();
    }

    @Test(dependsOnMethods = "openYandexMarket")
    @Description(value = "Open electronics")
    public void openElectronics() {
        testStep.stepOpenPage("electronics");
        testStep.checkElectronics();
    }

    @Test(dependsOnMethods = "openElectronics")
    @Description(value = "Open section {section}")
    public void openSection() {
        testStep.stepOpenPage(initData.getSection());
        testStep.checkSmartphones();
    }

    @Test(dependsOnMethods = "openSection")
    @Description(value = "Open filters")
    public void openFilters() {
        testStep.stepOpenPage("filters");
        testStep.checkFilters();
    }

    @Test(dependsOnMethods = "openFilters")
    @Description(value = "Apply filters")
    public void applyFilters() {
        testStep.applyPriceFrom(initData.getPrice());
        for (String producer : initData.getProducers()) {
            testStep.applyProducer(producer);
        }
        testStep.applyChangesFilter();
    }

    @Test(dependsOnMethods = "applyFilters")
    @Description(value = "Check filter result")
    public void checkFiltersResult() {
        testStep.checkCountElements(initData.getCount());
        String firstProduct = testStep.getFirstProduct();
        testStep.searchProduct(firstProduct);
        testStep.compareProduct(firstProduct);
    }
}
