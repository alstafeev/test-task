package models;

public class InitData {

    private String section;
    private String[] producers;
    private int price;
    private int count;

    public InitData(String section, String[] producers, int price, int count) {
        this.section = section;
        this.producers = producers;
        this.price = price;
        this.count = count;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String[] getProducers() {
        return producers;
    }

    public void setProducers(String[] producers) {
        this.producers = producers;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
