package models;

import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ElementsMap {

    private HashMap<String, Element> elementsMap = new HashMap<>();

    public HashMap<String, Element> getElementsMap() {
        return elementsMap;
    }

    public void setElementsMap(HashMap<String, Element> elementsMap) {
        this.elementsMap = elementsMap;
    }

    public static class Element {

        private String uid;
        private String xpath;
        private String type;
        private String link;

        public Element(String uid, String xpath, String type, String link) {
            this.uid = uid;
            this.xpath = xpath;
            this.type = type;
            this.link = link;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getXpath() {
            return xpath;
        }

        public void setXpath(String xpath) {
            this.xpath = xpath;
        }

        public By getElementById() {
            return By.id(uid);
        }

        public By getElementByXpath() {
            return By.xpath(xpath);
        }

        public By getElementByPartialLink() {
            return By.partialLinkText(link);
        }

        public By getElementByLink() {
            return By.linkText(link);
        }

        public By getElementByCss() {
            return By.cssSelector(genereateCss());
        }

        private String genereateCss() {
            return type + "[data-id=\"" + uid + "\"]";
        }
    }
}