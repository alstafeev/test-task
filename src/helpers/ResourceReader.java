package helpers;

import com.google.gson.Gson;
import models.ElementsMap;
import models.InitData;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class ResourceReader {

    public ElementsMap getElementsByTest(String test) throws Exception {
        String fileName = "/elementUids.json";
        String pathFile = "test/" + test + fileName;
        String resFile = getFileFromResourcesJSON(pathFile);
        Gson gson = new Gson();
        return gson.fromJson(resFile, ElementsMap.class);
    }

    public InitData[] getInitData(String filename) throws Exception {
        String resFile = getFileFromResourcesJSON(filename);
        Gson gson = new Gson();
        return gson.fromJson(resFile, InitData[].class);
    }

    private String getFileFromResourcesJSON(String pathFile) throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        try (InputStream inputStream = classLoader.getResourceAsStream(pathFile)) {
            assert inputStream != null;
            return IOUtils.toString(inputStream, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new IOException("Can not reads the file");
        }
    }
}
