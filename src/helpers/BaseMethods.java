package helpers;

import base.exception.*;
import base.exception.ElementNotVisibleException;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BaseMethods {
    private ChromeDriver webdriver;
    private WebDriverWait wait;

    public BaseMethods(ChromeDriver webdriver, int timeoutInSeconds) {
        this.webdriver = webdriver;
        this.wait = new WebDriverWait(webdriver, timeoutInSeconds);
    }

    public boolean openPage(String url) {
        try {
            webdriver.get(url);
            return true;
        } catch (WebDriverException err) {
            throw new PageNotOpenException("Page with url " + url + " not open");
        }
    }

    public boolean isElementExist(By selector) {
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
            if (webdriver.findElement(selector).isDisplayed()) {
                return true;
            }
            throw new ElementNotVisibleException("Element with selector " + selector.toString() + " not visible");
        } catch (TimeoutException err) {
            throw new ElementNotVisibleException("Element with selector " + selector.toString() + " not visible");
        }
    }

    public void isElementNotExist(By selector) {
        try {
            wait.until(ExpectedConditions.invisibilityOfElementLocated(selector));
        } catch (TimeoutException err) {
            throw new ElementVisibleException("Element with selector " + selector.toString() + " is visible");
        }
    }

    public boolean setInputValue(By selector, String value) {
        try {
            this.isElementExist(selector);
            webdriver.findElement(selector).sendKeys(value);
            return true;
        } catch (WebDriverException | ElementNotVisibleException err) {
            throw new ValueNotSetException("Value " + value + " not set in the input with selector " + selector.toString());
        }
    }

    public boolean clickElement(By selector) {
        try {
            this.isElementExist(selector);
            webdriver.findElement(selector).click();
            return true;
        } catch (WebDriverException | ElementNotVisibleException err) {
            throw new ElementNotClickException("Element with selector " + selector.toString() + " not click");
        }
    }

    public List<WebElement> findChildElements(By selector, String filter) {
        try {
            this.isElementExist(selector);
            return webdriver.findElement(selector).findElements(By.xpath(filter));
        } catch (WebDriverException | ElementNotVisibleException err) {
            throw new ChildElementNotFoundException("Child elements for selector - " + selector.toString() + " by filter - " + filter + " noy found");
        }
    }

    public String getElementText(By selector) {
        try {
            this.isElementExist(selector);
            return webdriver.findElement(selector).getText();
        } catch (WebDriverException | ElementNotVisibleException err) {
            throw new ElementTextNotExistException("Text on element with selector - " + selector.toString() + " noy found");
        }
    }
}