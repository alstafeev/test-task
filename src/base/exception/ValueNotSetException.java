package base.exception;

public class ValueNotSetException extends RuntimeException {

    public ValueNotSetException(String errorMessage) {
        super(errorMessage);
    }

}
