package base.exception;

public class ElementNotVisibleException extends RuntimeException {

    public ElementNotVisibleException(String errorMessage) {
        super(errorMessage);
    }

}
