package base.exception;

public class ElementVisibleException extends RuntimeException {

    public ElementVisibleException(String errorMessage) {
        super(errorMessage);
    }

}
