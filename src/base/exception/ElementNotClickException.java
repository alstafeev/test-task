package base.exception;

public class ElementNotClickException extends RuntimeException {

    public ElementNotClickException(String errorMessage) {
        super(errorMessage);
    }

}
