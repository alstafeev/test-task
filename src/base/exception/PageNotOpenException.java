package base.exception;

public class PageNotOpenException extends RuntimeException {

    public PageNotOpenException(String errorMessage) {
        super(errorMessage);
    }

}
