package base.exception;

public class ChildElementNotFoundException extends RuntimeException {

    public ChildElementNotFoundException(String errorMessage) {
        super(errorMessage);
    }

}