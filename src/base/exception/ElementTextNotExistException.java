package base.exception;

public class ElementTextNotExistException extends RuntimeException {

    public ElementTextNotExistException(String errorMessage) {
        super(errorMessage);
    }

}