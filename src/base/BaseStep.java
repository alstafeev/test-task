package base;

import base.exception.*;
import helpers.BaseMethods;
import io.qameta.allure.Step;
import models.ElementsMap;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.HashMap;
import java.util.List;

public abstract class BaseStep {

    protected BaseMethods baseMethods;
    protected HashMap<String, ElementsMap.Element> elementsUuid;

    public BaseStep(BaseMethods baseMethods, ElementsMap elementsMap) {
        this.baseMethods = baseMethods;
        elementsUuid = elementsMap.getElementsMap();
    }

    @Step(value = "Open url {url}")
    public void stepOpenUrl(String url) {
        try {
            Assert.assertTrue(baseMethods.openPage(url));
        } catch (PageNotOpenException err) {
            Assert.fail(err.getMessage());
        }
    }

    @Step(value = "Open {page} page")
    public void stepOpenMarket(String page) {
        try {
            Assert.assertTrue(baseMethods.clickElement(elementsUuid.get(page).getElementByCss()));
        } catch (ElementNotClickException err) {
            Assert.fail(err.getMessage());
        }
    }

    @Step(value = "Open {page} page")
    public void stepOpenPage(String page) {
        try {
            Assert.assertTrue(baseMethods.clickElement(elementsUuid.get(page).getElementByXpath()));
        } catch (ElementNotClickException err) {
            Assert.fail(err.getMessage());
        }
    }

    @Step(value = "Apply filter price from {price}")
    public void applyPriceFrom(int price) {
        try {
            Assert.assertTrue(baseMethods.setInputValue(elementsUuid.get("inputPriceFrom").getElementByXpath(), String.valueOf(price)));
        } catch (ValueNotSetException err) {
            Assert.fail(err.getMessage());
        }
    }

    @Step(value = "Apply filter producer to {producer}")
    public void applyProducer(String producer) {
        try {
            Assert.assertTrue(baseMethods.clickElement(By.linkText(producer)));
        } catch (ElementNotClickException err) {
            Assert.fail(err.getMessage());
        }
    }

    @Step(value = "Apply changes filter")
    public void applyChangesFilter() {
        try {
            Assert.assertTrue(baseMethods.clickElement(elementsUuid.get("apply").getElementByXpath()) &&
                    baseMethods.clickElement(elementsUuid.get("viewButton").getElementByXpath()));
        } catch (ElementNotClickException err) {
            Assert.fail(err.getMessage());
        }
    }

    @Step(value = "Check if count of elements is {count}")
    public void checkCountElements(int count) {
        try {
            baseMethods.isElementNotExist(By.xpath("//*[contains(@class, 'preloadable__paranja')]"));
            List<WebElement> elements = baseMethods.findChildElements(elementsUuid.get("products").getElementByXpath(), "//*[contains(@id, 'product-')]");
            Assert.assertEquals(elements.size(), count);
        } catch (ElementVisibleException | ChildElementNotFoundException err) {
            Assert.fail(err.getMessage());
        }
    }

    @Step(value = "Get first Product name element")
    public String getFirstProduct() {
        try {
            List<WebElement> elements = baseMethods.findChildElements(elementsUuid.get("products").getElementByXpath(), "//*[contains(@id, 'product-')]");
            return elements.get(0).findElement(By.xpath("//*[contains(@class, 'link n-link_theme_blue')]")).getText();
        } catch (ChildElementNotFoundException err) {
            Assert.fail(err.getMessage());
        }
        return null;
    }

    @Step(value = "Search product")
    public void searchProduct(String productName) {
        try {
            Assert.assertTrue(baseMethods.setInputValue(elementsUuid.get("inputSearch").getElementById(), productName) &&
                    baseMethods.clickElement(elementsUuid.get("buttonSearch").getElementByXpath()));
        } catch (ValueNotSetException | ElementNotClickException err) {
            Assert.fail(err.getMessage());
        }
    }

    @Step(value = "Compare product")
    public void compareProduct(String firstProduct) {
        try {
            baseMethods.clickElement(By.linkText(firstProduct));
            Assert.assertEquals(baseMethods.getElementText(elementsUuid.get("titleProduct").getElementByXpath()), firstProduct);
        } catch (ElementNotClickException | ElementTextNotExistException err) {
            Assert.fail(err.getMessage());
        }
    }

    @Step(value = "Checking visibility yandex page")
    public void checkYandexPage() {
        try {
            Assert.assertTrue(baseMethods.isElementExist(elementsUuid.get("market").getElementByCss()));
        } catch (ElementNotVisibleException err) {
            Assert.fail(err.getMessage());
        }
    }

    @Step(value = "Checking visibility yandex market")
    public void checkYandexMarket() {
        try {
            Assert.assertTrue(baseMethods.isElementExist(elementsUuid.get("electronics").getElementByXpath()));
        } catch (ElementNotVisibleException err) {
            Assert.fail(err.getMessage());
        }
    }

    @Step(value = "Checking visibility electronics page")
    public void checkElectronics() {
        try {
            Assert.assertTrue(baseMethods.isElementExist(elementsUuid.get("smartphones").getElementByXpath()));
        } catch (ElementNotVisibleException err) {
            Assert.fail(err.getMessage());
        }
    }

    @Step(value = "Checking visibility Smartphones page")
    public void checkSmartphones() {
        try {
            Assert.assertTrue(baseMethods.isElementExist(elementsUuid.get("filters").getElementByXpath()));
        } catch (ElementNotVisibleException err) {
            Assert.fail(err.getMessage());
        }
    }

}
