package base;

import helpers.BaseMethods;
import helpers.ResourceReader;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.*;


public abstract class BaseTest {

    private ChromeDriver webdriver;
    protected BaseMethods baseMethods;
    protected ResourceReader resourceReader;

    protected BaseTest() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/chromedriver");
        webdriver = new ChromeDriver(new ChromeOptions());
        webdriver.manage().window().maximize();
        baseMethods = new BaseMethods(webdriver, 15);
        resourceReader = new ResourceReader();
    }

    @AfterClass
    public void afterRun() {
        webdriver.close();
    }
}
